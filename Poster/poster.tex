\include{header}

\title{\Huge {Columnar packings of soft spheres in rotating fluids}}
\author{\Large{\underline{J.~Winkelmann}$^{a}$, D.B. Williams$^{a}$, A. Mughal$^{b}$, D.~Weaire$^{a}$ and S.~Hutzler$^{a}$}}
\subtitle{
\Large{$^{a}$School of Physics, Trinity College Dublin, The University of Dublin, Ireland\\
$^{b}$ Department of Mathematics, Aberystwyth University, Aberystwyth, Wales
}}
\date{}

\titlegraphic{%
  \includegraphics[clip=true, trim= 10 10 0 10,width=1.1\linewidth]{Abbildungen/trinity-stacked.jpg}
}

\institute{%
 \includegraphics[width=1.1\linewidth]{Abbildungen/foamarms.png}%
}

\begin{document}

\begin{columns}[T]
\column{0.96\textwidth}
\begin{tcolorbox}[
arc=10mm,
colback=white
]
\LARGE{
\bubble{
We present the simulation of ordered columnar packings of soft spheres based on enthalpy minimisation.
Remarkable similarities have been observed with particles in rotating fluids that self-assemble into such packings.
}
}
\end{tcolorbox}
\column{0.05\textwidth}
\centering
\includegraphics[width=\textwidth]{Abbildungen/qrcode.png}
\end{columns}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{block}{Self-assembly of spherical particles in rotating fluids (credits to T. Lee et al):}
\begin{columns}[T]
\column{0.53\textwidth}
\begin{columns}[c]
\column{0.4\textwidth}
\centering
\includegraphics[width=0.9\textwidth]{Abbildungen/Exp-Setup.png}

\column{0.6\textwidth}
\centering
\LARGE{\textbf{Experiment:}}
\Large{
\begin{itemize}
\item
Polymeric beads of mass $m$ suspended in a fluid of higher density
\item
Beads and fluid are then rotated with velocity $\omega$ inside a lathe
\end{itemize}
}
\LARGE{\textbf{Simulation:}}
\Large{
\begin{itemize}
\item
Computationally intensive molecular dynamics simulation to reproduce experiment
\item
"partially latching spring model" for bead interaction
\end{itemize}
}
\end{columns}

\column{0.47\textwidth}
\begin{columns}[c]
\column{0.5\textwidth}
\begin{itemize}
\item
Centripetal force moves beads to the center
\item
Rotational energy dependent on radial position $R$
\begin{equation}
E_{\text{rot}} = \frac{1}{2} m \omega^2 R^2
\end{equation}
\item
\textbf{Self-assembled packings remarkably similar to ordered uniform structures}
\item
\textcolor{blue}{Mixed structures} were observed
\end{itemize}

\column{0.5\textwidth}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=0.77\textwidth]{Abbildungen/ExpPacking.png}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\node at (1.16, 0.9) {\Large{$(2, 2, 0)$}};
\node at (1.16, 0.64) {\Large{$(3, 2, 1)$}};
\node at (1.16, 0.36) {\Large{$(3, 3, 0)$}};
\node at (1.16, 0.1) {\Large{$(4, 2, 2)$}};
\end{scope}
\end{tikzpicture}

\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=0.55\textwidth]{Abbildungen/mixed-structures.png}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\node at (1.35, 0.78) {\Large{$(3, 2, 1) / (4, 2, 2)$}};
\node at (1.35, 0.27) {\Large{$(3, 2, 1) / (4, 2, 2)$}};
\end{scope}
\end{tikzpicture}
\captionof{figure}{Different structures observed by Lee et al.}
\end{columns}
\end{columns}
\flushleft
\normalsize{
\href{http://onlinelibrary.wiley.com/doi/10.1002/adma.201704274/abstract}{
T. Lee, K. Gizynski, B.A. Grzybowski, \emph{Non-equilibrium Self-Assembly of Monocomponent and Multicomponent Tubular Structures in Rotating Fluids.} Adv. Mater. \textbf{29}, 1704274, (2017).}
}
\end{block}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{columns}[c]
\column{0.5\textwidth}
\begin{block}{Simulation based on enthalpy minimisation}%
\begin{columns}[T]
\column{0.41\textwidth}
\centering
\begin{figure}
%\includegraphics[width=0.9\linewidth]{Abbildungen/softinteract}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=\textwidth, clip=True, trim=250 0 250 0]{Abbildungen/321packing.png}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\draw[latex-, line width=1mm] (0.38, 0.82) -- (0.05, 0.71) node[below, text width=4cm] {image spheres};
\draw[-latex, line width=1mm] (0.05, 0.4) -- (0.38, 0.23);
\draw (0.7, 0.66) -- (0.9, 0.66); 
\draw (0.7, 0.36) -- (0.9, 0.36); 
\draw[latex-latex, line width=1mm] (0.8, 0.66) -- node[right] {$L$} (0.8, 0.36);
\draw (0.72, 0.6) -- (0.72, 1.1);
\draw (0.282, 0.6) -- (0.282, 1.1);
\draw[latex-latex, line width=1mm] (0.282, 1.) -- node[above] {$D$} (0.72, 1.);
\draw (0.55, 0.289) -- (0.9, 0.289);
\draw (0.55, 0.057) -- (0.9, 0.057);
\draw[latex-latex, line width=1mm] (0.8, 0.057) -- node[right] {$d$} (0.8, 0.289); 
\end{scope}
\end{tikzpicture}
\caption{\large{Unit cell (blue) with image spheres (red).}}
\end{figure}
\column{0.63\textwidth}
\begin{itemize}
\item
Enthalpy $H$:
\begin{equation}
\hspace{-2cm} H(\{\vec{r}_{i}\}, L) = \underbrace{\frac{1}{2} \sum_{ij}^N \varepsilon \delta_{ij}^2}_{\text{soft interaction}} + \underbrace{\frac{1}{2} \sum_i^N \varepsilon \rho_i^2}_{\text{wall confinement}} + \underbrace{pV}_{\text{pressure term}}
\end{equation}

\large{
\begin{itemize}
\item
Soft interaction depends on overlap $\delta_{ij}$
\item
Soft wall confinement depends on wall overlap $\rho_i$
\item
Pressure term $pV = p \pi \left(\frac{D}{2}\right)^2 L$
\end{itemize}
}
\Large{
\item
Periodic boundaries at top and bottom of unit cell
%image spheres twisted by angle $\alpha$
}
\end{itemize}
\textbf{
The Algorithm: Energy minimisation
}

\begin{itemize}
\item
Find energy minimum for given pressure $p$
\item
Basin-Hopping algorithm
\begin{itemize}
\item[--]
\alert{Global} Monte-Carlo type algorithm
\end{itemize}
\item
Conjugate Gradient algorithm
\begin{itemize}
\item[--]
\alert{Local} direct minimisation routine
\end{itemize}
\end{itemize}
\end{columns}

\end{block}%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{block}{What is a line-slip structure?}
\begin{columns}[b]
\column{0.25\textwidth}
\centering
\includegraphics[width=0.4\textwidth]{Abbildungen/321lineslip.png}
\captionof{figure}{Example of a line-slip packing.}
\column{0.25\textwidth}
\centering
\includegraphics[width=0.35\textwidth]{Abbildungen/Schematic321.png}
\captionof{figure}{Contact network of the line slip.}
\column{0.5\textwidth}
\centering
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=0.7\textwidth]{Abbildungen/rolled-out.pdf}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\draw[-latex, color=blue, line width=3mm] (0.464, 0.59) -- (0.67, 0.73);
\draw[latex-, color=red, line width=3mm] (0.52, 0.53) -- (0.72, 0.67);
\end{scope}
\end{tikzpicture}
\captionof{figure}{Rolled-out pattern of the contact network of the line slip.}
\end{columns}
\large{
\begin{itemize}
\item
Line slips differ from ordered uniform structures by a \alert{loss of contact}
\item
they are intervening structures between uniform structures
\item
Uniform packings are generated by sliding the \textcolor{red}{red} and \textcolor{blue}{blue} line along the arrows
\end{itemize}
}
\end{block}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\column{0.5\textwidth}

\begin{block}{Ordered uniform packings}
\vspace{-.7cm}
\begin{columns}[b]
\column{0.2\textwidth}
\centering
\includegraphics[width=0.82\textwidth, clip=true, trim=0 0 0 10]{Abbildungen/220}
\captionof{figure}{$(2, 2, 0)$}

\column{0.2\textwidth}
\centering
\includegraphics[width=0.85\textwidth]{Abbildungen/321}
\captionof{figure}{$(3, 2, 1)$}

\column{0.2\textwidth}
\centering
\includegraphics[width=0.97\textwidth, clip=true, trim=0 20 0 10]{Abbildungen/330}
\captionof{figure}{$(3, 3, 0)$}

\column{0.2\textwidth}
\centering
\includegraphics[width=0.85\textwidth, clip=true, trim=0 0 0 10]{Abbildungen/422}
\captionof{figure}{$(4, 2, 2)$}

\column{0.2\textwidth}
\centering
\includegraphics[width=0.85\textwidth, clip=true, trim=0 0 0 15]{Abbildungen/431}
\captionof{figure}{$(4, 3, 1)$}

\column{0.2\textwidth}
\end{columns}

\end{block}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{block}{The simulation and observation of a $(3, \bm{2}, 1)$ line slip}
\begin{columns}[T]
\column{0.45\textwidth}
\centering
\includegraphics[width=\textwidth]{Abbildungen/phasediagram.pdf}
\captionof{figure}{\normalsize{Computed phase diagram around the $(3, \bm{2}, 1)$ line slip. \\
(*) $(\bm{3}, 2, 1)$ line slip (expected by hard sphere limit) not visible because of finite pressure.}}
\column{0.55\textwidth}
\begin{columns}[c]
\column{0.4\textwidth}
\centering
\hspace{2mm}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=0.9\textwidth]{Abbildungen/expts.png}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\node at (0.15, 1.07) {\normalsize{$(3, 2, 1)$}};
\node at (0.47, 1.07) {\normalsize{$(3, \bm{2}, 1)$}};

\end{scope}
\end{tikzpicture}
\captionof{figure}{\normalsize{Exp. observation of a $(3, \bm{2}, 1)$ in wet foams.}}

\column{0.57\textwidth}
\centering
\includegraphics[width=0.9\textwidth, clip=true, trim=0 18 0 0]{Abbildungen/321_spirals_figure_1_again_cropped.jpg}
\captionof{figure}{\normalsize{Simulation and contact network of a $(3, \bm{2}, 1)$ line slip.}}
\end{columns}
\end{columns}

\begin{columns}[c]
\column{0.45\textwidth}
\flushright
\includegraphics[width=0.9\textwidth]{Abbildungen/EnthalpyConstP.pdf}

\column{0.55\textwidth}
\vspace{-3.3cm}
\begin{itemize}
\item
Red circle represents position of exp. observation of the $(3, \bm{2}, 1)$ line slip
\item
$p = 0$ corresponds to hard sphere limit \cite{mughal2012dense}  
\item
Continuous transitions:
\large{
\begin{itemize}
\item[--]
dashed lines in phase diagram
\item[--]
discontinuity in 2nd derivative of enthalpy
\end{itemize}
}
\item
\Large{discontinuous transitions:}
\large{
\begin{itemize}
\item[--]
solid lines in phase diagram
\item[--]
discontinuity in derivative of enthalpy
\end{itemize}
}
\end{itemize}
\end{columns}
\normalsize{
\href{https://journals.aps.org/pre/pdf/10.1103/PhysRevE.96.012610}{
J.~Winkelmann, B. Haffner, D. Weaire, A. Mughal and S. Hutzler, \emph{Simulation and observation of line-slip structures in columnar structures of soft spheres.} Phys Rev E \textbf{96}, 012610, (2017).}
}
\end{block}
\end{columns}

\begin{columns}
\column{0.45\textwidth}
\begin{block}{Stability maps for a reversible transition}
\centering
\begin{columns}[c]
\column{0.52\textwidth}
\begin{itemize}
\item
Packings can be stable outside of regions given in phase diagram
\item
Experiments show \textbf{different transition sequence} from phase diagram due to local equilibrium
\item
Stability regions depend on history of structure $\Rightarrow$ \textit{Hysteresis}
\item
\textit{Hysteresis} appears at the apex of the line slip region and above
\end{itemize}
\column{0.5\textwidth}
\centering
\large{
\textcolor{blue}{\textbf{Forward:} $(3, 2, 1) \Rightarrow (4, 2, 2)$} \\
\textcolor{red}{\textbf{Reverse:} $(4, 2, 2) \Rightarrow (3, 2, 1)$}
}
\includegraphics[width=0.9\textwidth]{Abbildungen/321to422Contour.pdf}
\captionof{figure}{\large{Stability regions for the $(3, 2, 1)$, $(3, \bm{2}, 1)$, and $(4, 2, 2)$ structures.}}
\end{columns}
\end{block}

\column{0.55\textwidth}
\begin{block}{Coming soon: Columnar structures in a harmonic potential}
\begin{tcolorbox}[
arc=5mm,
colback=tulight,
colframe=tudark,
boxrule=5pt,
]
\large{
Columnar sphere packings in rotating fluids can be simulated by confining the structures in a harmonic potential.
}
\end{tcolorbox}
\begin{columns}
\column{0.5\textwidth}
\centering
\LARGE{\textbf{Full Simulation:}}
\Large{
\begin{itemize}
\item
Total energy $E$:
\begin{equation}
E(\{\vec{r}_i\}) = \underbrace{\frac{1}{2}k \sum^N_{ij} \delta_{ij}^2}_{\text{\large soft interaction}} + \underbrace{\frac{1}{2} m \omega^2 R^2}_{\text{\large $E_{\text{rot}}$}}
\end{equation}
\item
Periodic boundaries at top and bottom
\item
Minimise energy for given tube length $L$
\item
Simulates finite size system
\item
Use Basin-Hopping algorithm
\end{itemize}
}
\column{0.5\textwidth}
\centering
\LARGE{\textbf{Semi-analytic approach:}}
\Large{
\begin{itemize}
\item
Calculate energy $E(R)$ for specific structure and minimise with respect to $R$
\end{itemize}
}
\vspace{-0.5cm}
\begin{columns}[T]
\column{0.53\textwidth}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=\textwidth]{Abbildungen/{Omega_1.0k_20.0}.pdf}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\draw[-latex, line width=0.5mm] (0.37, 0.3) node[below] {\small{mixed structure}} -- (0.43, 0.4);
\draw[latex-, line width=0.5mm] (0.515, 0.35) -- (0.58, 0.44) node[right] {\small{pure structure}};
\end{scope}
\end{tikzpicture}

\column{0.52\textwidth}
\Large{
\begin{itemize}
\item
Mixed structures lie on common tangent
\item
Calculation for infinite systems
\end{itemize}
}
\end{columns}
\end{columns}

\begin{tcolorbox}[
arc=5mm,
colback=tulight,
colframe=tudark,
boxrule=5pt,
]
\LARGE{
Do line slips appear in the self-assembly of spherical particles in rotating fluids?}
\end{tcolorbox}
\end{block}

\end{columns}

\vspace{-1cm}
%%%%%%%%%%%%%%%%%% REFERENCES %%%%%%%%%%%%%%%%%%%%%%%%
  \vspace*{\fill}
  \begin{columns}[b]
  \column{0.7\textwidth}
  \begin{block}[fonttitle=\normalsize, arc=5mm]{\vspace{-0.4cm} References \vspace{-0.7cm}}
    \begin{multicols}{3}
      \nocite{*}\footnotesize%
      \printbibliography%
    \end{multicols}
  \end{block}
  \vspace{-1.25cm}
  \column{0.13\textwidth}
  \includegraphics[width=\textwidth]{Abbildungen/sfi_logo.png}
  \column{0.17\textwidth}
  \includegraphics[width=\linewidth, clip=true, trim=325 17 0 20]{Abbildungen/Longlogo.jpg}
\end{columns}  

\end{document}
